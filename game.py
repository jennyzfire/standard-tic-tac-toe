def print_board(entries): #passing in a list [1, 2, 3, 4, 5, 6, 7, 8, 9]
    line = "+---+---+---+"
    output = line #output is also "+---+---+---+"
    n = 0 #n is zero
    for entry in entries: #for every iteration through the elements of [1, 2, 3, 4, 5, 6, 7, 8, 9]
        if n % 3 == 0: #if the current element is divisible by 3
            output = output + "\n| " #add a \n character with a | to output aka this makes the right border of the game
        else:
            output = output + " | " #or else the output has adds a |, making the divisors in the game i.e. 4 | 5 |
        output = output + str(entry)
        if n % 3 == 2: #if the current element divded by 3 gives remainder of 2:
            output = output + " |\n" #add a \n AFTER you add a |, so 2 |\n
            output = output + line #now you're making the horizontal line +---+---+---+
        n = n + 1 #incrementing n by 1
    print(output)
    print()

def game_over(board, num): #we need to pass something in this that makes board[num] accessible to any input 1-9
        print_board(board)
        print("GAME OVER") #printing out the current state in the board
        print(board[num], "has won") #print out the "symbol" aka board[num] "has won"
        exit()

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board) #passing in [1, 2, 3, 4, 5, 6, 7, 8, 9]
    response = input("Where would " + current_player + " like to move? ") #aka "X"
    space_number = int(response) - 1 #contains a number 1-9
    board[space_number] = current_player #swaps out the board element at the corresponding number with "X"

    if board[0] == board[1] and board[1] == board[2]: #if the board contains the same symbols in each of those locations
        game_over(board, space_number)
    elif board[3] == board[4] and board[4] == board[5]:
        game_over(board, space_number)
    elif board[6] == board[7] and board[7] == board[8]:
        game_over(board, space_number)
    elif board[0] == board[3] and board[3] == board[6]:
        game_over(board, space_number)
    elif board[1] == board[4] and board[4] == board[7]:
        game_over(board, space_number)
    elif board[2] == board[5] and board[5] == board[8]:
        game_over(board, space_number)
    elif board[0] == board[4] and board[4] == board[8]:
        game_over(board, space_number)
    elif board[2] == board[4] and board[4] == board[6]:
        game_over(board, space_number)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
